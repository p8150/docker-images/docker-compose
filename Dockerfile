FROM docker:20.10
ENV version="20.10"
RUN apk add --no-cache \
    docker-compose \
    docker-cli-buildx
LABEL version=${version}
